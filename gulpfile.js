var gulp = require("gulp");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var cleanCSS = require("gulp-clean-css");
var del = require("del");
var panini = require("panini");
const purgecss = require("gulp-purgecss");
var rollup = require("gulp-better-rollup");
var rollupBabel = require("rollup-plugin-babel");
var server = require("gulp-webserver");
var concatCss = require("gulp-concat-css");

var paths = {
  vendors: {
    src: "src/assets/vendors/**/*",
    dest: "dist/assets/vendors/",
  },
  styles: {
    src: "src/assets/scss/**/*.scss",
    dest: "dist/assets/css/",
  },
  scripts: {
    src: "src/assets/js/**/*.js",
    dest: "dist/assets/js/",
  },
  images: {
    src: "src/assets/images/*.{jpg,png,svg}",
    dest: "dist/assets/images/",
  },
};

/* Not all tasks need to use streams, a gulpfile is just another node program
 * and you can use all packages available on npm, but it must return either a
 * Promise, a Stream or take a callback and call it
 */
function clean() {
  // You can use multiple globbing patterns as you would with `gulp.src`,
  // for example if you are using del 2.0 or above, return its promise
  return del(["dist"]);
}

function vendors() {
  return gulp.src(paths.vendors.src).pipe(gulp.dest(paths.vendors.dest));
}

/*
 * Define our tasks using plain functions
 */
function scss() {
  return gulp
    .src(paths.styles.src)
    .pipe(sass().on("error", sass.logError))
    .pipe(
      purgecss({
        content: ["src/**/*.html"],
      })
    )
    .pipe(cleanCSS())
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest(paths.styles.dest));
}

function images() {
  return gulp.src(paths.images.src).pipe(gulp.dest(paths.images.dest));
}

function scripts() {
  return gulp
    .src(paths.scripts.src, { sourcemaps: true })
    .pipe(rollup({ plugins: [rollupBabel()] }, { format: "cjs" }))
    .pipe(uglify())
    .pipe(concat("main.min.js"))
    .pipe(gulp.dest(paths.scripts.dest));
}

function html() {
  return gulp
    .src("src/pages/**/*.html")
    .pipe(
      panini({
        root: "src/pages/",
        layouts: "src/layouts/",
        partials: "src/partials/",
        helpers: "src/helpers/",
        data: "src/data/",
      })
    )
    .pipe(gulp.dest("dist"));
}

function videos() {
  return gulp.src("src/assets/videos/*").pipe(gulp.dest("dist/assets/videos/"));
}

function fonts() {
  return gulp.src("src/assets/fonts/*").pipe(gulp.dest("dist/assets/fonts/"));
}

function watch() {
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, scss);
  gulp.watch(paths.images.src, images);
  gulp.watch("./src/assets/fonts/*", fonts);
  gulp.watch("./src/**/*.html", html);
  gulp.watch(
    ["./src/{layouts,partials,helpers,data,pages}/**/*"],
    panini.refresh
  );
}

function runServer() {
  return gulp.src("dist").pipe(
    server({
      livereload: true,
      open: true,
      port: 6001,
    })
  );
}

/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.series(
  clean,
  gulp.parallel(fonts, scss, scripts, html, images, vendors, videos, fonts)
);

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = clean;
exports.vendors = vendors;
exports.scss = scss;
exports.images = images;
exports.scripts = scripts;
exports.html = html;
exports.fonts = fonts;
exports.videos = videos;
exports.watch = watch;
exports.build = build;
exports.dev = gulp.series(build, gulp.parallel(runServer, watch));
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = build;
